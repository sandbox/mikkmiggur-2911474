import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
//import './index.css';

ReactDOM.render(
  <App />,
  document.getElementById('simple_elasticsearch_search_page_app')
);
